package pl.mydomain.michal.zadanie1.servlets;

import java.math.BigDecimal;
import java.net.URL;
import java.time.LocalDate;
import java.util.ArrayList;

public class Kredyt {

	private Integer kwota;
	private Integer iloscRat;
	private Double stopa; // w procentach
	private Integer oplata;
	private TypRaty typRaty = TypRaty.MALEJACA;

	private String kwotaStr;
	private String iloscRatStr;
	private String stopaStr;
	private String oplataStr;
	private String typRatyStr;

	private ArrayList<Rata> listaRat;
	
	public String[] headers = {"Nr raty", "Termin raty", "Kwota kapita�u", "Kwota odsetek", "Op�aty sta�e", "Kwota raty", "Saldo"}; 	
	

	public static void main(String[] args) {
		Kredyt k = new Kredyt();
		k.wyliczRatyMalejace();
		k.DrukujMojeRaty();
	}

	private void DrukujMojeRaty() {
		for (Rata r : listaRat) {
			System.out.println(r.toString());
		}
	}

	public boolean isValidate() {
		return isCorrectConversions() && (kwota > 0) && (iloscRat > 0)
				&& (stopa >= 0.0) && (oplata >= 0) && (typRaty != null);
	}

	private boolean isCorrectConversions() {
		try {
			this.kwota = Integer.parseInt(kwotaStr);
			this.iloscRat = Integer.parseInt(iloscRatStr);
			this.stopa = Double.parseDouble(stopaStr);
			this.oplata = Integer.parseInt(oplataStr);
			this.typRaty = (typRatyStr.equals("malejaca") ? TypRaty.MALEJACA
					: (typRatyStr.equals("stala") ? TypRaty.STALA : null));
			System.out.println("isCorrectConversions");
		} catch (Exception ex) {
			System.out.println("not isCorrectConversions");
			return false;
		}
		return true;
	}

	public void wyliczRatyMalejace() {
		this.listaRat = new ArrayList<Rata>();
		
		BigDecimal kapitalRaty = new BigDecimal(this.kwota.doubleValue() / this.iloscRat.doubleValue());
		BigDecimal kapitalZaplacony = new BigDecimal(0.0);
		Integer monthPeriod = 0; 
		
		BigDecimal saldo = new BigDecimal(this.kwota.doubleValue());
		while (saldo.compareTo(new BigDecimal(0)) > 0) {
			Rata r1 = new Rata();
			r1.setLp(this.listaRat.size()+1);
			r1.setTermin(java.sql.Date.valueOf(LocalDate.now().plusMonths(monthPeriod++)));
			r1.setKwotaOdsetek(saldo.multiply(new BigDecimal(stopa / 12 / 100)));
			r1.setKwotaKapitalu(kapitalRaty);
			r1.setOplatyStale(new BigDecimal(this.oplata.doubleValue()/this.iloscRat));
			r1.setKwotaRaty(kapitalRaty.add(r1.getKwotaOdsetek().add(r1.getOplatyStale())));
			kapitalZaplacony = kapitalRaty.multiply(new BigDecimal(listaRat
					.size()+1));
			saldo = new BigDecimal(this.kwota).subtract(kapitalZaplacony);
			if (saldo.compareTo(new BigDecimal(0)) < 0) {
				saldo = new BigDecimal(0);
			}
			r1.setSaldoKredytu(saldo);
			this.listaRat.add(r1);
			System.out.println(r1.toString());
		}
	}


	public int getKwota() {
		return kwota;
	}

	public void setKwota(int kwota) {
		this.kwota = kwota;
	}

	public int getIloscRat() {
		return iloscRat;
	}

	public void setIloscRat(int iloscRat) {
		this.iloscRat = iloscRat;
	}

	public double getStopa() {
		return stopa;
	}

	public void setStopa(double stopa) {
		this.stopa = stopa;
	}

	public int getOplata() {
		return oplata;
	}

	public void setOplata(int oplata) {
		this.oplata = oplata;
	}

	public TypRaty getTypRaty() {
		return typRaty;
	}

	public void setTypRaty(TypRaty typRaty) {
		this.typRaty = typRaty;
	}

	public String getKwotaStr() {
		return kwotaStr;
	}

	public void setKwotaStr(String kwotaStr) {
		this.kwotaStr = kwotaStr;
	}

	public String getIloscRatStr() {
		return iloscRatStr;
	}

	public void setIloscRatStr(String iloscRatStr) {
		this.iloscRatStr = iloscRatStr;
	}

	public String getStopaStr() {
		return stopaStr;
	}

	public void setStopaStr(String stopaStr) {
		this.stopaStr = stopaStr;
	}

	public String getOplataStr() {
		return oplataStr;
	}

	public void setOplataStr(String oplataStr) {
		this.oplataStr = oplataStr;
	}

	public String getTypRatyStr() {
		return typRatyStr;
	}

	public void setTypRatyStr(String typRatyStr) {
		this.typRatyStr = typRatyStr;
	}

	public ArrayList<Rata> getListaRat() {
		return listaRat;
	}

	public void setListaRat(ArrayList<Rata> listaRat) {
		this.listaRat = listaRat;
	}

	
}
