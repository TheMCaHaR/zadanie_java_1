package pl.mydomain.michal.zadanie1.servlets;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.security.Timestamp;
import java.util.Date;

@SuppressWarnings("unused")
public class Rata {
	private Integer lp; 
	private Date termin; 
	private BigDecimal kwotaKapitalu; 
	private BigDecimal kwotaOdsetek;
	private BigDecimal oplatyStale; 
	private BigDecimal kwotaRaty;
	private BigDecimal saldoKredytu;

	@Override
	public String toString() {
		return "Rata [termin=" + termin + ", kwotaKapitalu=" + kwotaKapitalu.setScale(2, RoundingMode.HALF_UP)
				+ ", kwotaOdsetek=" + kwotaOdsetek.setScale(2, RoundingMode.HALF_UP) 
				+ ", oplatyStale=" + oplatyStale.setScale(2, RoundingMode.HALF_UP) 
				+ ", kwotaRaty=" + kwotaRaty.setScale(2, RoundingMode.HALF_UP)
				+ ", saldoKredytu=" + saldoKredytu.setScale(2, RoundingMode.HALF_UP) + "]";
	}
	
	public Integer getLp() {
		return lp;
	}
	public void setLp(Integer lp) {
		this.lp = lp;
	}
	public Date getTermin() {
		return termin;
	}
	public void setTermin(Date timestamp) {
		this.termin = timestamp;
	}
	public BigDecimal getKwotaKapitalu() {
		return kwotaKapitalu;
	}
	public void setKwotaKapitalu(BigDecimal kwotaKapitalu) {
		this.kwotaKapitalu = kwotaKapitalu;
	}
	public BigDecimal getKwotaOdsetek() {
		return kwotaOdsetek;
	}
	public void setKwotaOdsetek(BigDecimal kwotaOdsetek) {
		this.kwotaOdsetek = kwotaOdsetek;
	}
	
	public BigDecimal getOplatyStale() {
		return oplatyStale;
	}

	public void setOplatyStale(BigDecimal oplatyStale) {
		this.oplatyStale = oplatyStale;
	}

	public BigDecimal getKwotaRaty() {	
		return this.kwotaRaty;
	}

	public BigDecimal getSaldoKredytu() {
		return saldoKredytu;
	}
	public void setSaldoKredytu(BigDecimal saldoKredytu) {
		this.saldoKredytu = saldoKredytu;
	}

	public void setKwotaRaty(BigDecimal kwotaRaty) {
		this.kwotaRaty = kwotaRaty; 
	}

}
