<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Request page</title>
</head>
<body>
	<h1>Super kredyt:</h1>
	
	<form action="kredytManager" method="post">
		<p>
			<label>Wnioskowana kwota kredytu: 
			<input type="text" id="kwota" name="kwota" value="6000" />
			</label>
		</p>
		<label>Ilość rat: <input type="text" id="iloscrat"
			name="iloscrat" value="10"/></label>
		<p>
			<label>Oprocentowanie: <input type="text" id="oprocentowanie"
				name="oprocentowanie" value="10.0"/></label>
		</p>
		<p>
			<label>Opłata stała: <input type="text" id="oplata"
				name="oplata" value="200"/>
			</label>
		</p>
		<p>
			<label>Rodzaj rat: <select name="rodzajrat">
					<option>malejaca</option>
					<option>stala</option>
			</select></label>
		</p>
		<input type="submit" value="symulacja" />
	</form>
</body>
</html>